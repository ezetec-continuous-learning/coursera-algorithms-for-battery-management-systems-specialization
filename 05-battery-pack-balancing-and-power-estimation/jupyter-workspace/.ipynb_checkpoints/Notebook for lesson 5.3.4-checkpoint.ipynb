{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Computing HPPC power limits\n",
    "The code in this notebook computes HPPC power limits based on knowledge of true SOC and also based on an SOC estimate (plus bounds) output by an SPFK.\n",
    "\n",
    "The code considers a dataset where profiles of power versus time were demanded from a cell, and the resulting data were used by an SPKF to estimate SOC. \"True\" SOC was also available from the lab equipment, since the test was calibrated in the ways you learned in the second course of the specialization.\n",
    "\n",
    "The code first computes effective resistances needed by the HPPC methods, then uses those in the HPPC method. Results are plotted at the end."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "% Parameters\n",
    "addpath ./readonly/ \n",
    "load('./readonly/CellModel.mat');               % ESC cell model\n",
    "load('./readonly/CellData.mat');                % time/current/voltage/soc\n",
    "load('./readonly/spkfData.mat');                % results of SPKF SOC estimation\n",
    "  \n",
    "% Cell Configuration\n",
    "T       = 25;                                   % (degC) ambient temperature\n",
    "Thorz   = 10;                                   % (s) horizon time\n",
    "z       = 0.5;                                  % [] initial cell SOC\n",
    "Ns      = 1;                                    % [] number of series cells\n",
    "Np      = 1;                                    % [] number of parallel cells\n",
    "\n",
    "% Operational Limits\n",
    "zmin = 0.1;     zmax = 0.9;                     % [] soc limits\n",
    "vmin = 2.8;     vmax = 4.3;                     % (V) voltage limits\n",
    "imin = -200;    imax = 350;                     % (A) current limits\n",
    "pmin = -inf;    pmax = inf;                     % (W) power limits\n",
    "\n",
    "% Effective Resistances\n",
    "Q    = getParamESC('QParam',T,model); \n",
    "iChg = 10*Q*[zeros(5,1); -ones(Thorz,1); zeros(5,1)];  % [A] charge pulse\n",
    "iDis = 10*Q*[zeros(5,1);  ones(Thorz,1); zeros(5,1)];  % [A] discharge pulse\n",
    "[vk,~,~,~,~] = simCell(iChg,T,model,1,z,0,0);\n",
    "dvChg = max(vk)-vk(1);\n",
    "iChg  = min(iChg);\n",
    "RChg  = abs(dvChg/iChg);\n",
    "fprintf('Rchg = %2.4f (mOhm)\\n',1000*RChg);\n",
    "\n",
    "[vk,~,~,~,~] = simCell(iDis,T,model,1,z,0,0);\n",
    "dvDis = min(vk)-vk(1);\n",
    "iDis  = max(iDis);\n",
    "RDis  = abs(dvDis/iDis);\n",
    "fprintf('Rdis = %2.4f (mOhm)\\n',1000*RDis);\n",
    "\n",
    "% HPPC Power Estimation: Truth\n",
    "OCV      = OCVfromSOCtemp(soc,T,model);\n",
    "iDisMaxV = (OCV-vmin)/RDis;\n",
    "iDisMaxZ = (soc - zmin)*3600*Q/10;\n",
    "iDisMax  = max(0,min([iDisMaxV;iDisMaxZ;imax*ones(size(soc))]));\n",
    "pDisMax  = min(vmin*iDisMax,pmax*ones(size(soc)));\n",
    "iChgMinV = (OCV-vmax)/RChg;\n",
    "iChgMinZ = (soc - zmax)*3600*Q/10;\n",
    "iChgMin  = max([iChgMinV;iChgMinZ;imin*ones(size(soc))]);\n",
    "pChgMin  = min(0,max(vmax*iChgMin,pmin*ones(size(soc))));\n",
    "HPPC.pDisMax = pDisMax;\n",
    "HPPC.pChgMin = pChgMin;\n",
    "\n",
    "% HPPC Power Estimation: SPKF\n",
    "OCVDis   = OCVfromSOCtemp(spkfSOC-bounds,T,model);\n",
    "OCVChg   = OCVfromSOCtemp(spkfSOC+bounds,T,model);\n",
    "iDisMaxV = (OCVDis-vmin)/RDis;\n",
    "iDisMaxZ = (spkfSOC-bounds - zmin)*3600*Q/10;\n",
    "iDisMax  = max(0,min([iDisMaxV;iDisMaxZ;imax*ones(size(soc))]));\n",
    "pDisMax  = min(vmin*iDisMax,pmax*ones(size(soc)));\n",
    "iChgMinV = (OCVChg-vmax)/RChg;\n",
    "iChgMinZ = (spkfSOC+bounds - zmax)*3600*Q/10;\n",
    "iChgMin  = max([iChgMinV;iChgMinZ;imin*ones(size(soc))]);\n",
    "pChgMin  = min(0,max(vmax*iChgMin,pmin*ones(size(soc))));\n",
    "spkfHPPC.pDisMax = pDisMax;\n",
    "spkfHPPC.pChgMin = pChgMin;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "% Discharge Power Estimation Figure\n",
    "subplot(2,1,1);\n",
    "plot(time/3600,HPPC.pDisMax/1000,'-','LineWidth',1.5); hold on\n",
    "plot(time/3600,spkfHPPC.pDisMax/1000,'r--','LineWidth',1.5);\n",
    "xlabel('Time (h)');\n",
    "ylabel('Power (kW)');\n",
    "title('Discharge power limit estimate');\n",
    "legend('True HPPC','SPKF HPPC','Location','SouthWest');\n",
    "grid on; hold off; ylim([-0.02 1.0])\n",
    "\n",
    "% Charge Power Estimation Figure\n",
    "subplot(2,1,2);\n",
    "plot(time/3600,HPPC.pChgMin/1000,'-','LineWidth',1.5); hold on\n",
    "plot(time/3600,spkfHPPC.pChgMin/1000,'r--','LineWidth',1.5);\n",
    "xlabel('Time (h)');\n",
    "ylabel('Power (kW)');\n",
    "title('Charge power limit estimate');\n",
    "legend('True HPPC','SPKF HPPC','Location','SouthWest');\n",
    "grid on; ylim([-1 0.02])"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Octave",
   "language": "octave",
   "name": "octave"
  },
  "language_info": {
   "file_extension": ".m",
   "help_links": [
    {
     "text": "MetaKernel Magics",
     "url": "https://github.com/calysto/metakernel/blob/master/metakernel/magics/README.md"
    }
   ],
   "mimetype": "text/x-octave",
   "name": "octave",
   "version": "0.16.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
