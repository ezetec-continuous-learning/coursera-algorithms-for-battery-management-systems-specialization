{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementation of EKF to estimate SOC using ESC model\n",
    "This notebook shows one way to implement an SOC estimator using an extended Kalman filter and an enhanced self-correcting cell model. The \"helper functions\" were the topic of lesson 3.4.6, and the main \"wrapper code\" was the topic of lesson 3.4.7."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "% First, make sure that the ESC toolbox functions are in the path\n",
    "addpath readonly"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following is the \"helper function\" used to initialize the EKF data structures."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "function ekfData = initEKF(v0,T0,SigmaX0,SigmaV,SigmaW,model)\n",
    "\n",
    "  % Initial state description\n",
    "  ir0   = 0;                           ekfData.irInd = 1;\n",
    "  hk0   = 0;                           ekfData.hkInd = 2;\n",
    "  SOC0  = SOCfromOCVtemp(v0,T0,model); ekfData.zkInd = 3;\n",
    "  ekfData.xhat  = [ir0 hk0 SOC0]'; % initial state\n",
    "\n",
    "  % Covariance values\n",
    "  ekfData.SigmaX = SigmaX0;\n",
    "  ekfData.SigmaV = SigmaV;\n",
    "  ekfData.SigmaW = SigmaW;\n",
    "  ekfData.Qbump = 5;\n",
    "  \n",
    "  % previous value of current\n",
    "  ekfData.priorI = 0;\n",
    "  ekfData.signIk = 0;\n",
    "  \n",
    "  % store model data structure too\n",
    "  ekfData.model = model;\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following is the \"helper function\" to update the EKF data structures every measurement interval."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "function [zk,zkbnd,ekfData] = iterEKF(vk,ik,Tk,deltat,ekfData)\n",
    "  model = ekfData.model;\n",
    "  % Load the cell model parameters\n",
    "  Q  = getParamESC('QParam',Tk,model);\n",
    "  G  = getParamESC('GParam',Tk,model);\n",
    "  M  = getParamESC('MParam',Tk,model);\n",
    "  M0 = getParamESC('M0Param',Tk,model);\n",
    "  RC = exp(-deltat./abs(getParamESC('RCParam',Tk,model)))';\n",
    "  R  = getParamESC('RParam',Tk,model)';\n",
    "  R0 = getParamESC('R0Param',Tk,model);\n",
    "  eta = getParamESC('etaParam',Tk,model);\n",
    "  if ik<0, ik=ik*eta; end;\n",
    "  \n",
    "  % Get data stored in ekfData structure\n",
    "  I = ekfData.priorI;\n",
    "  SigmaX = ekfData.SigmaX;\n",
    "  SigmaV = ekfData.SigmaV;\n",
    "  SigmaW = ekfData.SigmaW;\n",
    "  xhat = ekfData.xhat;\n",
    "  irInd = ekfData.irInd;\n",
    "  hkInd = ekfData.hkInd;\n",
    "  zkInd = ekfData.zkInd;\n",
    "  if abs(ik)>Q/100, ekfData.signIk = sign(ik); end;\n",
    "  signIk = ekfData.signIk;\n",
    "  \n",
    "  % EKF Step 0: Compute Ahat[k-1], Bhat[k-1]\n",
    "  nx = length(xhat); Ahat = zeros(nx,nx); Bhat = zeros(nx,1);\n",
    "  Ahat(zkInd,zkInd) = 1; Bhat(zkInd) = -deltat/(3600*Q);\n",
    "  Ahat(irInd,irInd) = diag(RC); Bhat(irInd) = 1-RC(:);\n",
    "  Ah  = exp(-abs(I*G*deltat/(3600*Q)));  % hysteresis factor\n",
    "  Ahat(hkInd,hkInd) = Ah;\n",
    "  B = [Bhat, 0*Bhat];\n",
    "  Bhat(hkInd) = -abs(G*deltat/(3600*Q))*Ah*(1+sign(I)*xhat(hkInd));\n",
    "  B(hkInd,2) = Ah-1;\n",
    "  \n",
    "  % Step 1a: State estimate time update\n",
    "  xhat = Ahat*xhat + B*[I; sign(I)]; \n",
    "  \n",
    "  % Step 1b: Error covariance time update\n",
    "  %          sigmaminus(k) = Ahat(k-1)*sigmaplus(k-1)*Ahat(k-1)' + ...\n",
    "  %                          Bhat(k-1)*sigmawtilde*Bhat(k-1)'\n",
    "  SigmaX = Ahat*SigmaX*Ahat' + Bhat*SigmaW*Bhat';\n",
    "  \n",
    "  % Step 1c: Output estimate\n",
    "  yhat = OCVfromSOCtemp(xhat(zkInd),Tk,model) + M0*signIk + ...\n",
    "         M*xhat(hkInd) - R*xhat(irInd) - R0*ik;\n",
    "  \n",
    "  % Step 2a: Estimator gain matrix\n",
    "  Chat = zeros(1,nx);\n",
    "  Chat(zkInd) = dOCVfromSOCtemp(xhat(zkInd),Tk,model);\n",
    "  Chat(hkInd) = M;\n",
    "  Chat(irInd) = -R;\n",
    "  Dhat = 1;\n",
    "  SigmaY = Chat*SigmaX*Chat' + Dhat*SigmaV*Dhat';\n",
    "  L = SigmaX*Chat'/SigmaY;\n",
    "  \n",
    "  % Step 2b: State estimate measurement update\n",
    "  r = vk - yhat; % residual.  Use to check for sensor errors...\n",
    "  if r^2 > 100*SigmaY, L(:)=0.0; end \n",
    "  xhat = xhat + L*r;\n",
    "  xhat(hkInd) = min(1,max(-1,xhat(hkInd))); % Help maintain robustness\n",
    "  xhat(zkInd) = min(1.05,max(-0.05,xhat(zkInd)));\n",
    "  \n",
    "  % Step 2c: Error covariance measurement update\n",
    "  SigmaX = SigmaX - L*SigmaY*L';\n",
    "  %   % Q-bump code\n",
    "  if r^2 > 4*SigmaY, % bad voltage estimate by 2 std. devs, bump Q \n",
    "    fprintf('Bumping SigmaX\\n');\n",
    "    SigmaX(zkInd,zkInd) = SigmaX(zkInd,zkInd)*ekfData.Qbump;\n",
    "  end\n",
    "  [~,S,V] = svd(SigmaX);\n",
    "  HH = V*S*V';\n",
    "  SigmaX = (SigmaX + SigmaX' + HH + HH')/4; % Help maintain robustness\n",
    "  \n",
    "  % Save data in ekfData structure for next time...\n",
    "  ekfData.priorI = ik;\n",
    "  ekfData.SigmaX = SigmaX;\n",
    "  ekfData.xhat = xhat;\n",
    "  zk = xhat(zkInd);\n",
    "  zkbnd = 3*sqrt(SigmaX(zkInd,zkInd));\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following \"wrapper\" code loads a lab-test data file, runs the EKF, and plots results. Make sure you type < shift >< enter> in the earlier \"helper function\" notebook cells before executing the \"wrapper code\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "% Load model file corresponding to a cell of this type\n",
    "% Has the variables: current, SOC, time, voltage\n",
    "load readonly/PANdata_P25.mat; % load data from Panasonic NMC cell, +25 degC\n",
    "T = 25; % Test temperature\n",
    "\n",
    "time    = DYNData.script1.time(:);   deltat = time(2)-time(1);\n",
    "time    = time-time(1); % start time at 0\n",
    "current = DYNData.script1.current(:); % discharge > 0; charge < 0.\n",
    "voltage = DYNData.script1.voltage(:);\n",
    "soc     = DYNData.script1.soc(:);\n",
    "\n",
    "% Load cell-test data to be used for this batch experiment\n",
    "% Contains variable \"DYNData\" of which the field \"script1\" is of \n",
    "% interest. This has sub-fields time, current, voltage, soc.\n",
    "load readonly/PANmodel.mat; % load ESC model of Panasonic NMC cell\n",
    "\n",
    "% Reserve storage for computed results, for plotting\n",
    "sochat = zeros(size(soc));\n",
    "socbound = zeros(size(soc));\n",
    "\n",
    "% Covariance values\n",
    "SigmaX0 = diag([1e2 1e-2 1e-3]); % uncertainty of initial state\n",
    "SigmaV = 3e-1; % Uncertainty of voltage sensor, output equation\n",
    "SigmaW = 4e0; % Uncertainty of current sensor, state equation\n",
    "\n",
    "% Create ekfData structure and initialize variables using first\n",
    "% voltage measurement and first temperature measurement\n",
    "ekfData = initEKF(voltage(1),T,SigmaX0,SigmaV,SigmaW,model);\n",
    "\n",
    "% Now, enter loop for remainder of time, where we update the SPKF\n",
    "% once per sample interval\n",
    "fprintf('Please be patient. This code will take several minutes to execute.\\n')\n",
    "for k = 1:length(voltage),\n",
    "  vk = voltage(k); % \"measure\" voltage\n",
    "  ik = current(k); % \"measure\" current\n",
    "  Tk = T;          % \"measure\" temperature\n",
    "  \n",
    "  % Update SOC (and other model states)\n",
    "  [sochat(k),socbound(k),ekfData] = iterEKF(vk,ik,Tk,deltat,ekfData);\n",
    "  if mod(k,1000)==0,\n",
    "    fprintf('  Completed %d out of %d iterations...\\n',k,length(voltage));\n",
    "  end  \n",
    "end\n",
    "  \n",
    "%%\n",
    "subplot(1,2,1); plot(time/60,100*sochat,time/60,100*soc); hold on\n",
    "plot([time/60; NaN; time/60],[100*(sochat+socbound); NaN; 100*(sochat-socbound)]);\n",
    "title('SOC estimation using EKF'); grid on\n",
    "xlabel('Time (min)'); ylabel('SOC (%)'); legend('Estimate','Truth','Bounds');\n",
    "\n",
    "%%\n",
    "fprintf('RMS SOC estimation error = %g%%\\n',sqrt(mean((100*(soc-sochat)).^2)));\n",
    "\n",
    "%%\n",
    "subplot(1,2,2); plot(time/60,100*(soc-sochat)); hold on\n",
    "plot([time/60; NaN; time/60],[100*socbound; NaN; -100*socbound]);\n",
    "title('SOC estimation errors using EKF');\n",
    "xlabel('Time (min)'); ylabel('SOC error (%)'); ylim([-4 4]); \n",
    "legend('Estimation error','Bounds'); \n",
    "grid on\n",
    "\n",
    "ind = find(abs(soc-sochat)>socbound);\n",
    "fprintf('Percent of time error outside bounds = %g%%\\n',length(ind)/length(soc)*100);"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Octave",
   "language": "octave",
   "name": "octave"
  },
  "language_info": {
   "file_extension": ".m",
   "help_links": [
    {
     "text": "MetaKernel Magics",
     "url": "https://github.com/calysto/metakernel/blob/master/metakernel/magics/README.md"
    }
   ],
   "mimetype": "text/x-octave",
   "name": "octave",
   "version": "0.16.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
