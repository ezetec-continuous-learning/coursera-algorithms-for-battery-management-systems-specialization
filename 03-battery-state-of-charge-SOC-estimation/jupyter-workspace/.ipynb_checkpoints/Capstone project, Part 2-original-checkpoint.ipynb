{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Capstone project, Part 2: Tuning an SPKF for SOC estimation\n",
    "Welcome to the second part of the capstone project! To complete this assignment, follow the instructions below. When you have completed your modifications to the sample code that I have provided as a starting point, click on the <b>\"Submit Assignment\"</b> button to submit your code to the grader."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Concept\n",
    "This Jupyter notebook operates in the same way as all of the other Jupyter notebooks that you have used so far in this specialization. You can type Octave code into notebook cells and execute that code to see how it functions. This allows you to test your code fully before submitting it for grading.\n",
    "\n",
    "Only one notebook cell is actually graded – the one marked with <code>\"% GRADED FUNCTION\"</code> in its first line. Do not modify that line – otherwise the grader will not be able to find the correct function for grading."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Deliverables\n",
    "The goal of Part 1 of the capstone project is to hand-tune an SPKF by selecting values for the covariances of process noise, sensor noise, and for the error of the initial SOC estimate. You will do this by trial-and-error to get the best result you are able to find. Some of the guidelines that you learned in the course will be helpful to you.\n",
    "\n",
    "When tuning an SPKF for a real application, these covariances are tuned so that the filter gives good and robust performance over a wide variety of operating conditions. However, for this project you will tune the filter to operate well for only a single operating scenario (otherwise, the project would take too long to complete).\n",
    "\n",
    "The scenario that you will be working with exercises a battery cell with an urban dynamometer driving cycle (UDDS). The cell is at an initial SOC of 95%. However, the EKF will assume an initial SOC estimate of 90%. So, part of the challenge in tuning the filter is to find covariance values that allow the filter to operate even with this initial error in the SOC estimate (which might be caused in practice by a poor initial voltage measurement, for example).\n",
    "\n",
    "Your deliverable will be a set of tuning covariance matrices within the <code>tuneSPKF</code> function, below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Implementation of SPKF to estimate SOC using ESC model\n",
    "This remainder of this notebook implements an SOC estimator using a sigma-point Kalman filter and an enhanced self-correcting cell model. The \"helper functions\" were the topic of lesson 3.4.6, and the main \"wrapper code\" was the topic of lesson 3.4.7. You will modify the new \"helper function\" named <code>tuneSPKF</code>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "% First, make sure that the ESC toolbox functions are in the path\n",
    "addpath readonly"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following is the \"helper function\" used to initialize the SPKF data structures. (Do not change this function.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "function spkfData = initSPKF(v0,T0,SigmaX0,SigmaV,SigmaW,model)\n",
    "\n",
    "  % Initial state description\n",
    "  ir0   = 0;                           spkfData.irInd = 1;\n",
    "  hk0   = 0;                           spkfData.hkInd = 2;\n",
    "  SOC0  = SOCfromOCVtemp(v0,T0,model); spkfData.zkInd = 3;\n",
    "  spkfData.xhat  = [ir0 hk0 SOC0]'; % initial state\n",
    "\n",
    "  % Covariance values\n",
    "  spkfData.SigmaX = SigmaX0;\n",
    "  spkfData.SigmaV = SigmaV;\n",
    "  spkfData.SigmaW = SigmaW;\n",
    "  spkfData.Snoise = real(chol(diag([SigmaW; SigmaV]),'lower'));\n",
    "  spkfData.Qbump = 5;\n",
    "  \n",
    "  % SPKF specific parameters\n",
    "  Nx = length(spkfData.xhat); spkfData.Nx = Nx; % state-vector length\n",
    "  Ny = 1; spkfData.Ny = Ny; % measurement-vector length\n",
    "  Nu = 1; spkfData.Nu = Nu; % input-vector length\n",
    "  Nw = size(SigmaW,1); spkfData.Nw = Nw; % process-noise-vector length\n",
    "  Nv = size(SigmaV,1); spkfData.Nv = Nv; % sensor-noise-vector length\n",
    "  Na = Nx+Nw+Nv; spkfData.Na = Na;     % augmented-state-vector length\n",
    "  \n",
    "  h = sqrt(3); h = 3;\n",
    "  spkfData.h = h; % SPKF/CDKF tuning factor  \n",
    "  Weight1 = (h*h-Na)/(h*h); % weighting factors when computing mean\n",
    "  Weight2 = 1/(2*h*h);      % and covariance\n",
    "  spkfData.Wm = [Weight1; Weight2*ones(2*Na,1)]; % mean\n",
    "  spkfData.Wc = spkfData.Wm;                     % covar\n",
    "\n",
    "  % previous value of current\n",
    "  spkfData.priorI = 0;\n",
    "  spkfData.signIk = 0;\n",
    "  \n",
    "  % store model data structure too\n",
    "  spkfData.model = model;\n",
    "end  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following is the \"helper function\" to update the SPKF data structures every measurement interval.  (Do not change this function.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "function [zk,zkbnd,spkfData] = iterSPKF(vk,ik,Tk,deltat,spkfData)\n",
    "  model = spkfData.model;\n",
    "\n",
    "  % Load the cell model parameters\n",
    "  Q  = getParamESC('QParam',Tk,model);\n",
    "  G  = getParamESC('GParam',Tk,model);\n",
    "  M  = getParamESC('MParam',Tk,model);\n",
    "  M0 = getParamESC('M0Param',Tk,model);\n",
    "  RC = exp(-deltat./abs(getParamESC('RCParam',Tk,model)))';\n",
    "  R  = getParamESC('RParam',Tk,model)';\n",
    "  R0 = getParamESC('R0Param',Tk,model);\n",
    "  eta = getParamESC('etaParam',Tk,model);\n",
    "  if ik<0, ik=ik*eta; end;\n",
    "  \n",
    "  % Get data stored in spkfData structure\n",
    "  I = spkfData.priorI;\n",
    "  SigmaX = spkfData.SigmaX;\n",
    "  xhat = spkfData.xhat;\n",
    "  Nx = spkfData.Nx;\n",
    "  Nw = spkfData.Nw;\n",
    "  Nv = spkfData.Nv;\n",
    "  Na = spkfData.Na;\n",
    "  Snoise = spkfData.Snoise;\n",
    "  Wc = spkfData.Wc;\n",
    "  irInd = spkfData.irInd;\n",
    "  hkInd = spkfData.hkInd;\n",
    "  zkInd = spkfData.zkInd;\n",
    "  if abs(ik)>Q/100, spkfData.signIk = sign(ik); end;\n",
    "  signIk = spkfData.signIk;\n",
    "  \n",
    "  % Step 1a: State estimate time update\n",
    "  %          - Create xhatminus augmented SigmaX points\n",
    "  %          - Extract xhatminus state SigmaX points\n",
    "  %          - Compute weighted average xhatminus(k)\n",
    "\n",
    "  % Step 1a-1: Create augmented SigmaX and xhat\n",
    "  [sigmaXa,p] = chol(SigmaX,'lower'); \n",
    "  if p>0,\n",
    "    fprintf('Cholesky error.  Recovering...\\n');\n",
    "    theAbsDiag = abs(diag(SigmaX));\n",
    "    sigmaXa = diag(max(SQRT(theAbsDiag),SQRT(spkfData.SigmaW)));\n",
    "  end\n",
    "  sigmaXa=[real(sigmaXa) zeros([Nx Nw+Nv]); zeros([Nw+Nv Nx]) Snoise];\n",
    "  xhata = [xhat; zeros([Nw+Nv 1])];\n",
    "  % NOTE: sigmaXa is lower-triangular\n",
    "\n",
    "  % Step 1a-2: Calculate SigmaX points (strange indexing of xhata to \n",
    "  % avoid \"repmat\" call, which is very inefficient in MATLAB)\n",
    "  Xa = xhata(:,ones([1 2*Na+1])) + ...\n",
    "       spkfData.h*[zeros([Na 1]), sigmaXa, -sigmaXa];\n",
    "\n",
    "  % Step 1a-3: Time update from last iteration until now\n",
    "  %     stateEqn(xold,current,xnoise)\n",
    "  Xx = stateEqn(Xa(1:Nx,:),I,Xa(Nx+1:Nx+Nw,:)); \n",
    "  xhat = Xx*spkfData.Wm;\n",
    "\n",
    "  % Step 1b: Error covariance time update\n",
    "  %          - Compute weighted covariance sigmaminus(k)\n",
    "  %            (strange indexing of xhat to avoid \"repmat\" call)\n",
    "  Xs = Xx - xhat(:,ones([1 2*Na+1]));\n",
    "  SigmaX = Xs*diag(Wc)*Xs';\n",
    "  \n",
    "  % Step 1c: Output estimate\n",
    "  %          - Compute weighted output estimate yhat(k)\n",
    "  I = ik; yk = vk;\n",
    "  Y = outputEqn(Xx,I,Xa(Nx+Nw+1:end,:),Tk,model);\n",
    "  yhat = Y*spkfData.Wm;\n",
    "\n",
    "  % Step 2a: Estimator gain matrix\n",
    "  Ys = Y - yhat(:,ones([1 2*Na+1]));\n",
    "  SigmaXY = Xs*diag(Wc)*Ys';\n",
    "  SigmaY = Ys*diag(Wc)*Ys';\n",
    "  L = SigmaXY/SigmaY; \n",
    "\n",
    "  % Step 2b: State estimate measurement update\n",
    "  r = yk - yhat; % residual.  Use to check for sensor errors...\n",
    "  if r^2 > 100*SigmaY, L(:,1)=0.0; end \n",
    "  xhat = xhat + L*r; \n",
    "  xhat(zkInd)=min(1.05,max(-0.05,xhat(zkInd)));\n",
    "  xhat(hkInd) = min(1,max(-1,xhat(hkInd)));\n",
    "\n",
    "  % Step 2c: Error covariance measurement update\n",
    "  SigmaX = SigmaX - L*SigmaY*L';\n",
    "  [~,S,V] = svd(SigmaX);\n",
    "  HH = V*S*V';\n",
    "  SigmaX = (SigmaX + SigmaX' + HH + HH')/4; % Help maintain robustness\n",
    "  \n",
    "  % Q-bump code\n",
    "  if r^2>4*SigmaY, % bad voltage estimate by 2-SigmaX, bump Q \n",
    "    fprintf('Bumping sigmax\\n');\n",
    "    SigmaX(zkInd,zkInd) = SigmaX(zkInd,zkInd)*spkfData.Qbump;\n",
    "  end\n",
    "  \n",
    "  % Save data in spkfData structure for next time...\n",
    "  spkfData.priorI = ik;\n",
    "  spkfData.SigmaX = SigmaX;\n",
    "  spkfData.xhat = xhat;\n",
    "  zk = xhat(zkInd);\n",
    "  zkbnd = 3*sqrt(SigmaX(zkInd,zkInd));\n",
    "  \n",
    "  % Calculate new states for all of the old state vectors in xold.  \n",
    "  function xnew = stateEqn(xold,current,xnoise)\n",
    "    current = current + xnoise; % noise adds to current\n",
    "    xnew = 0*xold;\n",
    "    xnew(irInd,:) = RC*xold(irInd,:) + (1-diag(RC))*current;\n",
    "    Ah = exp(-abs(current*G*deltat/(3600*Q)));  % hysteresis factor\n",
    "    xnew(hkInd,:) = Ah.*xold(hkInd,:) + (Ah-1).*sign(current);\n",
    "    xnew(zkInd,:) = xold(zkInd,:) - current/3600/Q;\n",
    "    xnew(hkInd,:) = min(1,max(-1,xnew(hkInd,:)));\n",
    "    xnew(zkInd,:) = min(1.05,max(-0.05,xnew(zkInd,:)));\n",
    "  end\n",
    "\n",
    "  % Calculate cell output voltage for all of state vectors in xhat\n",
    "  function yhat = outputEqn(xhat,current,ynoise,T,model)\n",
    "    yhat = OCVfromSOCtemp(xhat(zkInd,:),T,model);\n",
    "    yhat = yhat + M*xhat(hkInd,:) + M0*signIk;\n",
    "    yhat = yhat - R*xhat(irInd,:) - R0*current + ynoise(1,:);\n",
    "  end\n",
    "\n",
    "  % \"Safe\" square root\n",
    "  function X = SQRT(x)\n",
    "    X = sqrt(max(0,x));\n",
    "  end\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code provides the tuning values for the process-noise, sensor-noise, and initial SOC-estimation-error covariance matrices. You will tune the filter by changing the values in this function. \n",
    "\n",
    "Every time you make a change to these tuning variables, make sure that you type < shift >< enter > in this code, and then also press < shift >< enter > in the \"wrapper code\", below, that executes the EKF algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "% GRADED FUNCTION (do not modify this line)\n",
    "\n",
    "% function [SigmaW, SigmaV, SigmaZ0] = tuneEKF\n",
    "%\n",
    "% SigmaW - covariance value for current-sensor process noise\n",
    "% SigmaV - covariance value for voltage-sensor measurement noise\n",
    "% SigmaZ0 - covariance value for error in initial SOC estimate\n",
    "\n",
    "function [SigmaW, SigmaV, SigmaZ0] = tuneSPKF\n",
    "\n",
    "  % BEGIN MODIFYING CODE AFTER THIS\n",
    "  SigmaW  = 0.1; % This is a sample value. You will need to change it.\n",
    "  SigmaV  = 0.1; % This is a sample value. You will need to change it.\n",
    "  SigmaZ0 = 0.1; % This is a sample value. You will need to change it.\n",
    "     \n",
    "end  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following \"wrapper\" code loads a lab-test data file, runs the SPKF, and plots results. Make sure you type < shift >< enter> in the earlier \"helper function\" notebook cells before executing the \"wrapper code\".  (Do not change this code.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "% Load model file corresponding to a cell of this type\n",
    "% Has the variables: current, SOC, time, voltage\n",
    "load readonly/PAN_CAPSTONE_DATA.mat; % load data from Panasonic NMC cell, +25 degC\n",
    "T = 25; % Test temperature\n",
    "\n",
    "time    = DYNData.script1.time(:);   deltat = time(2)-time(1);\n",
    "time    = time-time(1); % start time at 0\n",
    "current = DYNData.script1.current(:); % discharge > 0; charge < 0.\n",
    "voltage = DYNData.script1.voltage(:);\n",
    "soc     = DYNData.script1.soc(:);\n",
    "\n",
    "% Load cell-test data to be used for this batch experiment\n",
    "% Contains variable \"DYNData\" of which the field \"script1\" is of \n",
    "% interest. This has sub-fields time, current, voltage, soc.\n",
    "load readonly/PANmodel.mat; % load ESC model of Panasonic NMC cell\n",
    "\n",
    "% Reserve storage for computed results, for plotting\n",
    "sochat = zeros(size(soc));\n",
    "socbound = zeros(size(soc));\n",
    "\n",
    "% Get tuning values from user-modified function\n",
    "[SigmaW, SigmaV, SigmaZ0] = tuneSPKF;\n",
    "\n",
    "SigmaX0 = diag([1e-6 1e-6 SigmaZ0]);\n",
    "spkfData = initSPKF(voltage(1),T,SigmaX0,SigmaV,SigmaW,model);\n",
    "% This simulation tests the SPKF when there is an inital SOC-estimation error\n",
    "% The true initial SOC is 95%, but we will initialize the SOC estimate in the \n",
    "% filter to 90% and see how quickly and well the filter converges toward the\n",
    "% correct SOC.\n",
    "spkfData.xhat(spkfData.zkInd)=0.90; % \n",
    "\n",
    "% Now, enter loop for remainder of time, where we update the SPKF\n",
    "% once per sample interval\n",
    "fprintf('Please be patient. This code will take a minute or so to execute.\\n')\n",
    "for k = 1:length(voltage),\n",
    "  vk = voltage(k); % \"measure\" voltage\n",
    "  ik = current(k); % \"measure\" current\n",
    "  Tk = T;          % \"measure\" temperature\n",
    "  \n",
    "  % Update SOC (and other model states)\n",
    "  [sochat(k),socbound(k),spkfData] = iterSPKF(vk,ik,Tk,deltat,spkfData);\n",
    "  % update waitbar periodically, but not too often (slow procedure)\n",
    "  if mod(k,300)==0,\n",
    "    fprintf('  Completed %d out of %d iterations...\\n',k,length(voltage));\n",
    "  end  \n",
    "end\n",
    "\n",
    "%%\n",
    "subplot(1,2,1); plot(time/60,100*sochat,time/60,100*soc); hold on\n",
    "plot([time/60; NaN; time/60],[100*(sochat+socbound); NaN; 100*(sochat-socbound)]);\n",
    "title('SOC estimation using SPKF'); grid on\n",
    "xlabel('Time (min)'); ylabel('SOC (%)'); legend('Estimate','Truth','Bounds');\n",
    "\n",
    "%%\n",
    "J1 = sqrt(mean((100*(soc-sochat)).^2));\n",
    "fprintf('RMS SOC estimation error = %g%%\\n',J1);\n",
    "\n",
    "%%\n",
    "J2 = 100*socbound(end);\n",
    "fprintf('Final value of SOC estimation error bounds = %g%%\\n',J2);\n",
    "\n",
    "%%\n",
    "subplot(1,2,2); plot(time/60,100*(soc-sochat)); hold on\n",
    "plot([time/60; NaN; time/60],[100*socbound; NaN; -100*socbound],'--');\n",
    "title('SOC estimation errors using EKF');\n",
    "xlabel('Time (min)'); ylabel('SOC error (%)'); ylim([-4 4]); \n",
    "legend('Estimation error','Bounds'); \n",
    "grid on\n",
    "\n",
    "ind = find(abs(soc-sochat)>socbound);\n",
    "fprintf('Percent of time error outside bounds = %g%%\\n',length(ind)/length(soc)*100);\n",
    "\n",
    "% Compute the prospective grade\n",
    "tableRow = min(11,ceil(max(0,J1-0.1)/0.01 + 1));\n",
    "tableCol = min(11,ceil(max(0,J2-0.2)/0.02 + 1));\n",
    "table = hankel([10:-1:0]);\n",
    "grade = table(tableRow,tableCol);\n",
    "if ~isempty(ind),\n",
    "  fprintf('Your SOC estimation error was sometimes outside of bounds, so your overall grade is 0/10.');\n",
    "else\n",
    "  fprintf('Your grade is calculated from row %d and column %d of the grading table that is\\n',tableRow,tableCol);\n",
    "  fprintf('listed in the project description page. This will result in a grade of %d/10.\\n',grade);\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "When you are satisfied with your tuning, click on the \"<b>Submit Assignment</b>\" button, above.\n",
    "\n",
    "The assignment will be graded out of a maximum of 10 points possible. Part of the grade depends on the root-mean-squared SOC estimation error of your tuned filter. Part of the grade depends on the final value of the 3-sigma bounds on the SOC estimate. Part of the grade depends on whether your estimate is ever outside of the 3-sigma bounds.\n",
    "* If your code produces errors when executed, the grade is zero\n",
    "* If your SOC estimation error is ever outside the 3-sigma bounds, the grade is zero\n",
    "* Otherwise, your grade can be found looking up the RMS SOC estimation error and the final value of the 3-sigma bounds in the grading table given in the project description page (the value is also computed by this code, for easy reference).\n",
    " * For a grade of 100%, you will need to achieve an RMS SOC estimation error of less than 0.1% and a final SOC esimtation error bound of less than 0.2%\n",
    "\n",
    "If you are curious, the best results that I have been able to obtain are:\n",
    "* RMS SOC estimation error = 0.098%\n",
    "* Final value of SOC estimation error bounds = 0.197%\n",
    "\n",
    "It took me quite a long time to find these good results, and I also discovered that the SPKF is far more sensitive to one of its tuning values than the other two. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "coursera": {
   "course_slug": "battery-state-of-charge",
   "graded_item_id": "ssQk5",
   "launcher_item_id": "epmP1"
  },
  "kernelspec": {
   "display_name": "Octave",
   "language": "octave",
   "name": "octave"
  },
  "language_info": {
   "file_extension": ".m",
   "help_links": [
    {
     "text": "GNU Octave",
     "url": "https://www.gnu.org/software/octave/support.html"
    },
    {
     "text": "Octave Kernel",
     "url": "https://github.com/Calysto/octave_kernel"
    },
    {
     "text": "MetaKernel Magics",
     "url": "https://github.com/calysto/metakernel/blob/master/metakernel/magics/README.md"
    }
   ],
   "mimetype": "text/x-octave",
   "name": "octave",
   "version": "4.2.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
